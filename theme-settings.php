<?php declare(strict_types = 1);

/**
 * @file
 * Theme settings form for whitecanvas theme.
 */

use Drupal\Core\Form\FormState;

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function whitecanvas_form_system_theme_settings_alter(array &$form, FormState $form_state): void {

  $form['whitecanvas'] = [
    '#type' => 'details',
    '#title' => t('whitecanvas'),
    '#open' => TRUE,
  ];

  $form['whitecanvas']['example'] = [
    '#type' => 'textfield',
    '#title' => t('Example'),
    '#default_value' => theme_get_setting('example'),
  ];

}
